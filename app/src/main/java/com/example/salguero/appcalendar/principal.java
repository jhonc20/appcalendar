package com.example.salguero.appcalendar;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

public class principal extends ActionBarActivity implements View.OnClickListener {

    private static final int ALARM_REQUEST_CODE = 1;
    //ProgressDialog dialog;
    private Datos objDatos;
    private boolean estado;
    private Button btnEnero, btnFebrero, btnMarzo, btnAbril, btnMayo, btnJunio, btnJulio, btnAgosto, btnSeptiembre, btnOctubre, btnNoviembre, btnDiciembre;
    private ImageView btnBono;
    private Handler updateBarHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.principal);
        cargarInformacion();
        if (!estado) {
            objDatos = new Datos(this);
            ventanaModal();
            new procesoCargaMes().execute();
        }
        inicio();
        notification();
    }

    private void notification() {
        AlarmManager manager = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);

        Intent intent = new Intent(this, MyReceiver.class);
        PendingIntent pIntent = PendingIntent.getBroadcast(this, ALARM_REQUEST_CODE, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.add(Calendar.SECOND, 60);
        manager.setRepeating(manager.RTC_WAKEUP, calendar.getTimeInMillis(), 50 * 1000, pIntent);
        btnBono = (ImageView) findViewById(R.id.btnBono);
        btnBono.setOnClickListener(this);
    }

    private void inicio() {
        btnEnero = (Button) findViewById(R.id.btnEnero);
        btnFebrero = (Button) findViewById(R.id.btnFebrero);
        btnMarzo = (Button) findViewById(R.id.btnMarzo);
        btnAbril = (Button) findViewById(R.id.btnAbril);
        btnMayo = (Button) findViewById(R.id.btnMayo);
        btnJunio = (Button) findViewById(R.id.btnJunio);
        btnJulio = (Button) findViewById(R.id.btnJulio);
        btnAgosto = (Button) findViewById(R.id.btnAgosto);
        btnSeptiembre = (Button) findViewById(R.id.btnSeptiembre);
        btnOctubre = (Button) findViewById(R.id.btnOctubre);
        btnNoviembre = (Button) findViewById(R.id.btnNoviembre);
        btnDiciembre = (Button) findViewById(R.id.btnDiciembre);

        btnEnero.setOnClickListener(this);
        btnFebrero.setOnClickListener(this);
        btnMarzo.setOnClickListener(this);
        btnAbril.setOnClickListener(this);
        btnMayo.setOnClickListener(this);
        btnJunio.setOnClickListener(this);
        btnJulio.setOnClickListener(this);
        btnAgosto.setOnClickListener(this);
        btnSeptiembre.setOnClickListener(this);
        btnOctubre.setOnClickListener(this);
        btnNoviembre.setOnClickListener(this);
        btnDiciembre.setOnClickListener(this);

        btnEnero.setTag("1");
        btnFebrero.setTag("2");
        btnMarzo.setTag("3");
        btnAbril.setTag("4");
        btnMayo.setTag("5");
        btnJunio.setTag("6");
        btnJulio.setTag("7");
        btnAgosto.setTag("8");
        btnSeptiembre.setTag("9");
        btnOctubre.setTag("10");
        btnNoviembre.setTag("11");
        btnDiciembre.setTag("12");

    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(this, actividad.class);
        switch (v.getId()) {
            case R.id.btnEnero:
                intent.putExtra("id", btnEnero.getTag().toString());
                startActivity(intent);
                break;
            case R.id.btnFebrero:
                intent.putExtra("id", btnFebrero.getTag().toString());
                startActivity(intent);
                break;
            case R.id.btnMarzo:
                intent.putExtra("id", btnMarzo.getTag().toString());
                startActivity(intent);
                break;
            case R.id.btnAbril:
                intent.putExtra("id", btnAbril.getTag().toString());
                startActivity(intent);
                break;
            case R.id.btnMayo:
                intent.putExtra("id", btnMayo.getTag().toString());
                startActivity(intent);
                break;
            case R.id.btnJunio:
                intent.putExtra("id", btnJunio.getTag().toString());
                startActivity(intent);
                break;
            case R.id.btnJulio:
                intent.putExtra("id", btnJulio.getTag().toString());
                startActivity(intent);
                break;
            case R.id.btnAgosto:
                intent.putExtra("id", btnAgosto.getTag().toString());
                startActivity(intent);
                break;
            case R.id.btnSeptiembre:
                intent.putExtra("id", btnSeptiembre.getTag().toString());
                startActivity(intent);
                break;
            case R.id.btnOctubre:
                intent.putExtra("id", btnOctubre.getTag().toString());
                startActivity(intent);
                break;
            case R.id.btnNoviembre:
                intent.putExtra("id", btnNoviembre.getTag().toString());
                startActivity(intent);
                break;
            case R.id.btnDiciembre:
                intent.putExtra("id", btnDiciembre.getTag().toString());
                startActivity(intent);
                break;
            case R.id.btnBono:
                ventanaBonoEducativo();
                break;
        }
    }

    private void ventanaBonoEducativo() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        ImageView img = new ImageView(this);
        img.setBackgroundResource(R.drawable.abono_educativo);
        alert.setTitle("ABONO EDUCATIVO");
        alert.setView(img);
        alert.show();
    }

    private void ventanaModal() {
        final ProgressDialog barProgressDialog = new ProgressDialog(principal.this);
        barProgressDialog.setTitle("Espere un momento...");
        barProgressDialog.setMessage("Cargando Informacion Necesaria");

        barProgressDialog.setProgressStyle(barProgressDialog.STYLE_HORIZONTAL);
        barProgressDialog.setProgress(0);
        barProgressDialog.setMax(100);
        barProgressDialog.setCancelable(false);
        barProgressDialog.show();
        updateBarHandler = new Handler();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    while (barProgressDialog.getProgress() <= barProgressDialog.getMax()) {
                        Thread.sleep(1000);
                        updateBarHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                barProgressDialog.incrementProgressBy(10);
                            }
                        });
                        if (barProgressDialog.getProgress() == barProgressDialog.getMax())
                            barProgressDialog.dismiss();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private void cargarInformacion() {
        SharedPreferences informacion = getSharedPreferences("Informacion", Context.MODE_PRIVATE);
        estado = informacion.getBoolean("isLoad", false);
    }

    private void guardarInformacion(boolean valor) {
        SharedPreferences informacion = getSharedPreferences("Informacion", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = informacion.edit();
        editor.putBoolean("isLoad", valor);
        editor.commit();
    }

    private class procesoCargaModalidad extends AsyncTask<Void, Void, Void> {

        ArrayList<clsModalidad> objModalidad = new ArrayList<clsModalidad>(Arrays.asList(
                new clsModalidad(1, "Presencial"),
                new clsModalidad(2, "Horario Tabajo")
        ));

        @Override
        protected void onPreExecute() {
            /*dialog = new ProgressDialog(principal.this);
            dialog.setTitle("Cargando Modalidad");
            dialog.setMessage("Cargando Modalidad");
            dialog.setCancelable(false);
            dialog.show();*/
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            guardarInformacion(true);
            //if (dialog.isShowing()) {
            //  dialog.dismiss();
            new procesoCargaTipoActividad().execute();
            //}
        }

        @Override
        protected Void doInBackground(Void... params) {
            for (int i = 0; i < objModalidad.size(); i++) {
                clsModalidad oModalidad = objModalidad.get(i);
                objDatos.insertarDatos(oModalidad);
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }
    }

    private class procesoCargaTipoActividad extends AsyncTask<Void, Void, Void> {

        ArrayList<clsTipoActividad> objTipoActividad = new ArrayList<clsTipoActividad>(Arrays.asList(
                new clsTipoActividad(1, "Inicio de Modulo Presencial"),
                new clsTipoActividad(2, "Inicio de Modulo Horario Trabajo"),
                new clsTipoActividad(3, "Examen Parcial"),
                new clsTipoActividad(4, "Finalizacion de Modulo Presencial"),
                new clsTipoActividad(5, "Finalizacion de Modulo Horario Trabajo"),
                new clsTipoActividad(6, "Fecha Limite de Pago Presencial"),
                new clsTipoActividad(7, "Fecha Limite de Pago Horario Trabajo"),
                new clsTipoActividad(8, "Descanso Pedagogico"),
                new clsTipoActividad(9, "Feriado"),
                new clsTipoActividad(10, "Aniversario UPDS")
        ));

        @Override
        protected void onPreExecute() {
            /*dialog = new ProgressDialog(principal.this);
            dialog.setTitle("Cargando Tipo Actividad");
            dialog.setMessage("Cargando Tipo Actividad");
            dialog.setCancelable(false);
            dialog.show();*/
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            guardarInformacion(true);
            //if (dialog.isShowing()) {
            //   dialog.dismiss();
            new procesoCargaActividad().execute();
            //}
        }

        @Override
        protected Void doInBackground(Void... params) {
            for (int i = 0; i < objTipoActividad.size(); i++) {
                clsTipoActividad oTipoActividad = objTipoActividad.get(i);
                objDatos.insertarDatos(oTipoActividad);
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }
    }

    private class procesoCargaMes extends AsyncTask<Void, Void, Void> {
        ArrayList<clsMes> objMes = new ArrayList<clsMes>(Arrays.asList(
                new clsMes(1, "Enero"),
                new clsMes(2, "Febrero"),
                new clsMes(3, "Marzo"),
                new clsMes(4, "Abril"),
                new clsMes(5, "Mayo"),
                new clsMes(6, "Junio"),
                new clsMes(7, "Julio"),
                new clsMes(8, "Agosto"),
                new clsMes(9, "Septiembre"),
                new clsMes(10, "Octubre"),
                new clsMes(11, "Noviembre"),
                new clsMes(12, "Diciembre")
        ));

        @Override
        protected void onPreExecute() {
            /*dialog = new ProgressDialog(principal.this);
            dialog.setTitle("Cargando Mes");
            dialog.setMessage("Cargando Mes");
            dialog.setCancelable(false);
            dialog.show();*/
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            guardarInformacion(true);
            //if (dialog.isShowing()) {
            //  dialog.dismiss();
            new procesoCargaModalidad().execute();
            //    }
        }

        @Override
        protected Void doInBackground(Void... params) {
            for (int i = 0; i < objMes.size(); i++) {
                clsMes oMes = objMes.get(i);
                objDatos.insertarDatos(oMes);
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }
    }

    private class procesoCargaActividad extends AsyncTask<Void, Void, Void> {

        ArrayList<clsActividad> objActividad = new ArrayList<>(Arrays.asList(
                new clsActividad(1, 1, 1, 9, 3, "2015-01-01"),
                new clsActividad(2, 1, 1, 8, 3, "2015-01-02"),
                new clsActividad(3, 1, 1, 1, 3, "2015-01-07"),
                new clsActividad(4, 1, 2, 2, 3, "2015-01-10"),
                new clsActividad(5, 1, 1, 3, 3, "2015-01-19"),
                new clsActividad(6, 1, 1, 4, 3, "2015-01-20"),
                new clsActividad(7, 1, 1, 9, 3, "2015-01-22"),
                new clsActividad(8, 1, 2, 7, 3, "2015-01-24"),
                new clsActividad(9, 1, 2, 5, 3, "2015-01-31"),
                new clsActividad(10, 2, 1, 4, 3, "2015-02-03"),
                new clsActividad(11, 2, 1, 1, 3, "2015-02-04"),
                new clsActividad(12, 2, 2, 2, 3, "2015-02-07"),
                new clsActividad(13, 2, 1, 9, 3, "2015-02-16"),
                new clsActividad(14, 2, 1, 3, 3, "2015-02-19"),
                new clsActividad(15, 2, 1, 6, 3, "2015-02-20"),
                new clsActividad(16, 2, 2, 4, 3, "2015-02-21"),
                new clsActividad(17, 2, 2, 5, 3, "2015-02-28"),
                new clsActividad(18, 3, 1, 4, 3, "2015-03-05"),
                new clsActividad(19, 3, 1, 1, 3, "2015-03-06"),
                new clsActividad(20, 3, 2, 2, 3, "2015-03-07"),
                new clsActividad(21, 3, 1, 3, 3, "2015-03-19"),
                new clsActividad(22, 3, 1, 6, 3, "2015-03-20"),
                new clsActividad(23, 3, 2, 7, 3, "2015-03-21"),
                new clsActividad(24, 3, 2, 5, 3, "2015-03-28"),
                new clsActividad(25, 4, 1, 4, 3, "2015-04-02"),
                new clsActividad(26, 4, 1, 9, 3, "2015-04-03"),
                new clsActividad(27, 4, 2, 8, 3, "2015-04-04"),
                new clsActividad(28, 4, 1, 1, 3, "2015-04-06"),
                new clsActividad(29, 4, 2, 2, 3, "2015-04-11"),
                new clsActividad(30, 4, 1, 3, 3, "2015-04-15"),
                new clsActividad(31, 4, 1, 6, 3, "2015-04-16"),
                new clsActividad(32, 4, 2, 7, 3, "2015-04-25"),
                new clsActividad(33, 5, 1, 9, 3, "2015-05-01"),
                new clsActividad(34, 5, 2, 5, 3, "2015-05-02"),
                new clsActividad(35, 5, 1, 4, 3, "2015-05-04"),
                new clsActividad(36, 5, 1, 1, 3, "2015-05-05"),
                new clsActividad(37, 5, 2, 2, 3, "2015-05-09"),
                new clsActividad(38, 5, 1, 3, 3, "2015-05-19"),
                new clsActividad(39, 5, 1, 6, 3, "2015-05-20"),
                new clsActividad(40, 5, 2, 7, 3, "2015-05-23"),
                new clsActividad(41, 5, 2, 5, 3, "2015-05-30"),
                new clsActividad(42, 6, 1, 4, 3, "2015-06-01"),
                new clsActividad(43, 6, 1, 1, 3, "2015-06-02"),
                new clsActividad(44, 6, 1, 9, 3, "2015-06-04"),
                new clsActividad(45, 6, 2, 2, 3, "2015-06-06"),
                new clsActividad(46, 6, 1, 3, 3, "2015-06-16"),
                new clsActividad(47, 6, 1, 6, 3, "2015-06-17"),
                new clsActividad(48, 6, 2, 7, 3, "2015-06-20"),
                new clsActividad(49, 6, 1, 9, 3, "2015-06-22"),
                new clsActividad(50, 6, 2, 5, 3, "2015-06-27"),
                new clsActividad(51, 7, 1, 4, 3, "2015-07-01"),
                new clsActividad(52, 7, 1, 1, 3, "2015-07-02"),
                new clsActividad(53, 7, 2, 2, 3, "2015-07-04"),
                new clsActividad(54, 7, 1, 3, 3, "2015-07-15"),
                new clsActividad(55, 7, 1, 6, 3, "2015-07-16"),
                new clsActividad(56, 7, 2, 7, 3, "2015-07-18"),
                new clsActividad(57, 7, 2, 5, 3, "2015-07-25"),
                new clsActividad(58, 7, 1, 4, 3, "2015-07-29"),
                new clsActividad(59, 7, 1, 8, 3, "2015-07-30"),
                new clsActividad(60, 8, 2, 8, 3, "2015-08-01"),
                new clsActividad(61, 8, 1, 1, 3, "2015-08-03"),
                new clsActividad(62, 8, 1, 9, 3, "2015-08-06"),
                new clsActividad(63, 8, 2, 2, 3, "2015-08-08"),
                new clsActividad(64, 8, 1, 3, 3, "2015-08-18"),
                new clsActividad(65, 8, 1, 6, 3, "2015-08-19"),
                new clsActividad(66, 8, 2, 7, 3, "2015-08-22"),
                new clsActividad(67, 8, 2, 5, 3, "2015-08-29"),
                new clsActividad(68, 8, 1, 4, 3, "2015-08-31"),
                new clsActividad(69, 9, 1, 1, 3, "2015-09-01"),
                new clsActividad(70, 9, 2, 2, 3, "2015-09-05"),
                new clsActividad(71, 9, 1, 10, 3, "2015-09-15"),
                new clsActividad(72, 9, 1, 3, 3, "2015-09-18"),
                new clsActividad(73, 9, 2, 7, 3, "2015-09-19"),
                new clsActividad(74, 9, 1, 6, 3, "2015-09-21"),
                new clsActividad(75, 9, 2, 5, 3, "2015-09-26"),
                new clsActividad(76, 9, 1, 4, 3, "2015-09-29"),
                new clsActividad(77, 9, 1, 1, 3, "2015-09-30"),
                new clsActividad(78, 10, 2, 2, 3, "2015-10-03"),
                new clsActividad(79, 10, 1, 3, 3, "2015-10-16"),
                new clsActividad(80, 10, 2, 7, 3, "2015-10-17"),
                new clsActividad(81, 10, 1, 6, 3, "2015-10-19"),
                new clsActividad(82, 10, 2, 5, 3, "2015-10-24"),
                new clsActividad(83, 10, 1, 4, 3, "2015-10-27"),
                new clsActividad(84, 10, 1, 1, 3, "2015-10-28"),
                new clsActividad(85, 10, 2, 2, 3, "2015-10-31"),
                new clsActividad(86, 11, 1, 9, 3, "2015-11-02"),
                new clsActividad(87, 11, 1, 3, 3, "2015-11-13"),
                new clsActividad(88, 11, 2, 7, 3, "2015-11-14"),
                new clsActividad(89, 11, 1, 6, 3, "2015-11-16"),
                new clsActividad(90, 11, 2, 5, 3, "2015-11-21"),
                new clsActividad(91, 11, 1, 4, 3, "2015-11-25"),
                new clsActividad(92, 11, 1, 1, 3, "2015-11-26"),
                new clsActividad(93, 11, 2, 2, 3, "2015-11-28"),
                new clsActividad(94, 12, 1, 3, 3, "2015-12-11"),
                new clsActividad(95, 12, 2, 7, 3, "2015-12-12"),
                new clsActividad(96, 12, 1, 6, 3, "2015-12-14"),
                new clsActividad(97, 12, 2, 5, 3, "2015-12-19"),
                new clsActividad(98, 12, 1, 4, 3, "2015-12-22"),
                new clsActividad(99, 12, 1, 8, 3, "2015-12-23"),
                new clsActividad(100, 12, 1, 9, 3, "2015-12-25")
        ));

        @Override
        protected void onPreExecute() {
            /*dialog = new ProgressDialog(principal.this);
            dialog.setTitle("Cargando Actividad");
            dialog.setMessage("Cargando Actividad");
            dialog.setCancelable(false);
            dialog.show();*/
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            guardarInformacion(true);
            //  if (dialog.isShowing()) {
            // dialog.dismiss();
            //}
        }

        @Override
        protected Void doInBackground(Void... params) {
            for (int i = 0; i < objActividad.size(); i++) {
                clsActividad oActividad = objActividad.get(i);
                objDatos.insertarDatos(oActividad);
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }
    }
}
