package com.example.salguero.appcalendar;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.sql.Date;
import java.util.ArrayList;

public class Datos extends SQLiteOpenHelper {

    private static final String DB_NAME = "calendario";
    private static final int VERSION = 1;
    private SQLiteDatabase db;

    public Datos(Context context) {
        super(context, DB_NAME, null, VERSION);
        this.db = this.getWritableDatabase();
    }

    public void cerrarConexion() {
        this.db.close();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(clsMes.CREATE_TABLE);
        db.execSQL(clsModalidad.CREATE_TABLE);
        db.execSQL(clsTipoActividad.CREATE_TABLE);
        db.execSQL(clsActividad.CREATE_TABLE);
    }

    private ContentValues generarValoresMes(clsMes objMes) {
        ContentValues valores = new ContentValues();
        valores.put(clsMes.ID, objMes.getId());
        valores.put(clsMes.NOMBRE_MES, objMes.getNombreMes());
        return valores;
    }

    private ContentValues generarValoresModalidad(clsModalidad objModalidad) {
        ContentValues valores = new ContentValues();
        valores.put(clsModalidad.ID, objModalidad.getId());
        valores.put(clsModalidad.NOMBRE_MODALIDAD, objModalidad.getNombreModalidad());
        return valores;
    }

    private ContentValues generarValoresTipoActivida(clsTipoActividad objTipoActividad) {
        ContentValues valores = new ContentValues();
        valores.put(clsTipoActividad.ID, objTipoActividad.getId());
        valores.put(clsTipoActividad.NOMBRE_ACTIVIDAD, objTipoActividad.getNombreActividad());
        return valores;
    }

    private ContentValues generarValoresActividad(clsActividad objActividad) {
        ContentValues valores = new ContentValues();
        valores.put(clsActividad.ID, objActividad.getId());
        valores.put(clsActividad.ID_MES, objActividad.getIdMes());
        valores.put(clsActividad.ID_MODALIDAD, objActividad.getIdModalidad());
        valores.put(clsActividad.ID_TIPO_ACTIVIDAD, objActividad.getIdTipoActividad());
        valores.put(clsActividad.DIA, objActividad.getDia());
        valores.put(clsActividad.FECHA, String.valueOf(objActividad.getFecha()));
        return valores;
    }

    public void insertarDatos(clsMes objMes) {
        db.insert(clsMes.TABLE_NAME, null, generarValoresMes(objMes));
    }

    public void insertarDatos(clsModalidad objModalidad) {
        db.insert(clsModalidad.TABLE_NAME, null, generarValoresModalidad(objModalidad));
    }

    public void insertarDatos(clsTipoActividad objTipoActividad) {
        db.insert(clsTipoActividad.TABLE_NAME, null, generarValoresTipoActivida(objTipoActividad));
    }

    public void insertarDatos(clsActividad objActividad) {
        db.insert(clsActividad.TABLE_NAME, null, generarValoresActividad(objActividad));
    }

    public clsMes datosMes(String id) {
        clsMes aux = null;
        Cursor cursor = db.rawQuery("SELECT * FROM mes WHERE _id=" + id, null);
        if (cursor.moveToFirst()) {
            do {
                aux = new clsMes();
                aux.setId(cursor.getInt(0));
                aux.setNombreMes(cursor.getString(1));

            } while (cursor.moveToNext());
        }
        cursor.close();
        return aux;
    }

    public ArrayList<clsConsultaActividades> datosActividad(String id) {
        ArrayList<clsConsultaActividades> aux = new ArrayList<>();
        Cursor c = db.rawQuery("select actividad.fecha,tipoActividad.nombreActividad  from actividad inner join mes on actividad.idMes = mes._id inner join modalidad on modalidad._id = actividad.idModalidad inner join tipoActividad on tipoActividad._id = actividad.idTipoActividad where mes._id = " + id, null);
        if (c.moveToFirst()) {
            do {
                clsConsultaActividades cls = new clsConsultaActividades();
                cls.setFecha(c.getString(0));
                cls.setNombreActividad(c.getString(1));
                aux.add(cls);
            } while (c.moveToNext());
        }
        c.close();
        return aux;
    }

    public String informacionActividad(Date fecha) {
        String csql = "select tipoActividad.nombreActividad,actividad.fecha from actividad inner join tipoActividad on actividad.idTipoActividad=tipoActividad._id where actividad.fecha = '" + fecha.toString() + "'";
        Cursor c = db.rawQuery(csql, null);
        if (c.moveToFirst()) {
            do {
                return c.getString(0) + " el " + c.getString(1);
            } while (c.moveToNext());
        }
        return "";
    }

    public boolean datosActividad(Date fecha) {
        String csql = "select * from actividad where fecha = '" + fecha.toString() + "'";
        Cursor c = db.rawQuery(csql, null);
        return c.moveToFirst();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
