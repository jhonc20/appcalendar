package com.example.salguero.appcalendar;

public class clsTipoActividad {

    public static final String TABLE_NAME = "tipoActividad";

    public static final String ID = "_id";
    public static final String NOMBRE_ACTIVIDAD = "nombreActividad";

    public static final String CREATE_TABLE = "create table " + TABLE_NAME + " ("
            + ID + " INTEGER PRIMARY KEY,"
            + NOMBRE_ACTIVIDAD + " TEXT NOT NULL);";

    private int id;
    private String nombreActividad;

    public clsTipoActividad(int id, String nombreActividad) {
        this.id = id;
        this.nombreActividad = nombreActividad;
    }

    public clsTipoActividad() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombreActividad() {
        return nombreActividad;
    }

    public void setNombreActividad(String nombreActividad) {
        this.nombreActividad = nombreActividad;
    }
}
