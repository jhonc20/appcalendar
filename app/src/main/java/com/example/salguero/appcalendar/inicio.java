package com.example.salguero.appcalendar;

import android.app.Activity;
import android.content.Intent;
import android.os.CountDownTimer;
import android.os.Bundle;


public class inicio extends Activity {

    public static final int segundos = 5;
    public static final int milisegundos = segundos * 1000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inicio);
        empezarTiempo();
    }

    private CountDownTimer tiempo = null;
    private void empezarTiempo() {
        tiempo = new CountDownTimer(milisegundos, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {
                Intent i = new Intent(inicio.this,principal.class);
                finish();
                startActivity(i);
            }
        }.start();
    }
}
