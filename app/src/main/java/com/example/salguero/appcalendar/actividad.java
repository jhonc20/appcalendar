package com.example.salguero.appcalendar;

import android.app.NotificationManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;


public class actividad extends ActionBarActivity {

    Datos obj;
    private TextView lMes;
    private ListView listaActividad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actividad);
        inicio();
    }

    private void inicio() {
        obj = new Datos(this);
        listaActividad = (ListView) findViewById(R.id.lista);
        lMes = (TextView) findViewById(R.id.lMes);
        Bundle parametro = getIntent().getExtras();
        if (parametro != null) {
            cargarInformacion(parametro.getString("id"));
            cerrarNotification();
        }
    }

    private void cerrarNotification() {
        String ns = Context.NOTIFICATION_SERVICE;
        NotificationManager nm = (NotificationManager) this.getSystemService(ns);
        nm.cancel(1);
    }

    private void cargarInformacion(String param) {
        clsMes aux = obj.datosMes(param);
        lMes.setText(aux.getNombreMes().toString().toUpperCase());
        cargarActividades(param);
    }

    private void cargarActividades(String param) {
        ArrayAdapter<clsConsultaActividades> adapter = new ArrayAdapter<>(actividad.this, android.R.layout.simple_list_item_1, obj.datosActividad(param));
        listaActividad.setAdapter(adapter);
        obj.cerrarConexion();
    }
}
