package com.example.salguero.appcalendar;

public class clsConsultaActividades {
    private String fecha, nombreActividad;

    public clsConsultaActividades() {

    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getNombreActividad() {
        return nombreActividad;
    }

    public void setNombreActividad(String nombreActividad) {
        this.nombreActividad = nombreActividad;
    }

    public String toString() {
        return this.fecha.trim().substring(8) + " | " + this.nombreActividad;
    }
}
