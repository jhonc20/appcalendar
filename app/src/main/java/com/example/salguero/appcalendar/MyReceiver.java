package com.example.salguero.appcalendar;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;

import java.sql.Date;
import java.util.Calendar;

public class MyReceiver extends BroadcastReceiver {

    private static final int ID_NOTIFICACION = 1;
    Datos objDatos;
    NotificationManager nm;
    private Calendar c;
    private int mesAux;

    @Override
    public void onReceive(Context context, Intent intent) {

        objDatos = new Datos(context);

        c = Calendar.getInstance();
        int anio = c.get(Calendar.YEAR);
        int mes = c.get(Calendar.MONTH);
        mes += 1;
        int dia = c.get(Calendar.DAY_OF_MONTH);
        Date fechaPrevia = Date.valueOf(fechaPrevia(anio, mes, dia));

        int hora = c.get(Calendar.HOUR);
        int minuto = c.get(Calendar.MINUTE);

        if (objDatos.datosActividad(fechaPrevia)) {
            if (hora == 11) {
                if (minuto ==00)
                    notificacion(context, fechaPrevia);
            }
        }
        objDatos.cerrarConexion();
    }

    private void notificacion(Context context, Date fecha) {
        Datos d = new Datos(context);
        nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification = new Notification(R.drawable.icono, " U - Calendar ", System.currentTimeMillis());
        Bitmap icono = BitmapFactory.decodeResource(Resources.getSystem(), R.drawable.icono);
        notification.largeIcon = icono;
        long[] pattern = new long[]{1000, 500, 1000};
        notification.vibrate = pattern;
        //Uri sonido = RingtoneManager.getDefaultUri(Notification.DEFAULT_SOUND);
        Uri sonido = Uri.parse("android.resource://com.example.salguero.appcalendar/" + R.raw.tono);
        notification.sound = sonido;

        notification.flags = notification.flags | Notification.FLAG_SHOW_LIGHTS;
        Intent act = new Intent(context.getApplicationContext(), actividad.class);

        act.putExtra("id", String.valueOf(mesAux));
        PendingIntent intencionPendiente = PendingIntent.getActivity(context.getApplicationContext(), 0, act, 0);

        notification.setLatestEventInfo(context.getApplicationContext(), " U - Calendar ", d.informacionActividad(fecha), intencionPendiente);

        nm.notify(ID_NOTIFICACION, notification);
        d.cerrarConexion();
    }

    private String fechaPrevia(int anio, int mes, int dia) {
        String fechaPrevista = "";
        dia += 3;
        if (mes == 4 || mes == 6 || mes == 9 || mes == 11) {
            if (dia <= 30) {
                fechaPrevista = anio + "-" + mes + "-" + dia;
                mesAux = mes;
            } else {
                fechaPrevista = anio + "-" + (mes + 1) + "-" + (dia - 30);
                mesAux = mes + 1;
            }
        }
        if (mes == 1 || mes == 3 || mes == 5 || mes == 7 || mes == 8 || mes == 10 || mes == 12) {
            if (dia <= 31) {
                fechaPrevista = anio + "-" + mes + "-" + dia;
                mesAux = mes;
            } else {
                fechaPrevista = anio + "-" + (mes + 1) + "-" + (dia - 31);
                mesAux = mes + 1;
            }
        }
        if (mes == 2) {
            if (anio % 4 == 0 && anio % 100 == 0 && anio % 400 == 0) {
                if (dia <= 29) {
                    fechaPrevista = anio + "-" + mes + "-" + dia;
                    mesAux = mes;
                } else {
                    fechaPrevista = anio + "-" + (mes + 1) + "-" + (dia - 29);
                    mesAux = mes + 1;
                }
            } else {
                if (dia <= 28) {
                    fechaPrevista = anio + "-" + mes + "-" + dia;
                    mesAux = mes;
                } else {
                    fechaPrevista = anio + "-" + (mes + 1) + "-" + (dia - 28);
                    mesAux = mes + 1;
                }
            }
        }
        return fechaPrevista;
    }
}
