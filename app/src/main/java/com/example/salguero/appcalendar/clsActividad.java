package com.example.salguero.appcalendar;

public class clsActividad {

    public static final String TABLE_NAME = "actividad";

    public static final String ID = "_id";
    public static final String ID_MES = "idMes";
    public static final String ID_MODALIDAD = "idModalidad";
    public static final String ID_TIPO_ACTIVIDAD = "idTipoActividad";
    public static final String FECHA = "fecha";
    public static final String DIA = "dia";

    public static final String CREATE_TABLE = "create table " + TABLE_NAME + " ("
            + ID + " INTEGER PRIMARY KEY,"
            + ID_MES + " INTEGER REFERENCES mes(_id),"
            + ID_MODALIDAD + " INTEGER REFERENCES modalidad(_id),"
            + ID_TIPO_ACTIVIDAD + " INTEGER REFERENCES tipoActividad(_id),"
            + FECHA + " DATE NOT NULL,"
            + DIA + " INTEGER NOT NULL);";

    private int id, idMes, idModalidad, idTipoActividad, dia;
    private String fecha;

    public clsActividad(int id, int idMes, int idModalidad, int idTipoActividad, int dia, String fecha) {
        this.id = id;
        this.idMes = idMes;
        this.idModalidad = idModalidad;
        this.idTipoActividad = idTipoActividad;
        this.dia = dia;
        this.fecha = fecha;
    }

    public clsActividad() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdMes() {
        return idMes;
    }

    public void setIdMes(int idMes) {
        this.idMes = idMes;
    }

    public int getIdModalidad() {
        return idModalidad;
    }

    public void setIdModalidad(int idModalidad) {
        this.idModalidad = idModalidad;
    }

    public int getIdTipoActividad() {
        return idTipoActividad;
    }

    public void setIdTipoActividad(int idTipoActividad) {
        this.idTipoActividad = idTipoActividad;
    }

    public int getDia() {
        return dia;
    }

    public void setDia(int dia) {
        this.dia = dia;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
}
