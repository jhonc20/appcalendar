package com.example.salguero.appcalendar;

public class clsModalidad {

    public static final String TABLE_NAME = "modalidad";

    public static final String ID = "_id";
    public static final String NOMBRE_MODALIDAD = "nombreModalidad";

    public static final String CREATE_TABLE = "create table " + TABLE_NAME + " ("
            + ID + " INTEGER PRIMARY KEY,"
            + NOMBRE_MODALIDAD + " TEXT NOT NULL);";

    private int id;
    private String nombreModalidad;

    public clsModalidad() {
    }

    public clsModalidad(int id, String nombreModalidad) {
        this.id = id;
        this.nombreModalidad = nombreModalidad;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombreModalidad() {
        return nombreModalidad;
    }

    public void setNombreModalidad(String nombreModalidad) {
        this.nombreModalidad = nombreModalidad;
    }
}
