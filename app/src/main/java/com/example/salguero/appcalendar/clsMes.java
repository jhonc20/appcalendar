package com.example.salguero.appcalendar;


public class clsMes {
    public static final String TABLE_NAME = "mes";

    public static final String ID = "_id";
    public static final String NOMBRE_MES = "nombreMes";

    public static final String CREATE_TABLE = "create table " + TABLE_NAME + " ("
            + ID + " INTEGER PRIMARY KEY,"
            + NOMBRE_MES + " TEXT NOT NULL);";

    private int id;
    private String nombreMes;

    public clsMes(int id, String nombreMes) {
        this.id = id;
        this.nombreMes = nombreMes;
    }

    public clsMes() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombreMes() {
        return nombreMes;
    }

    public void setNombreMes(String nombreMes) {
        this.nombreMes = nombreMes;
    }
}
